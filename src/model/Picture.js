/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A picture is an image took by an author, made available freely by a given provider.
 * It has coordinates associated, and some other metadata.
 */
class Picture {
//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param {string} pictureUrl The picture URL.
	 * @param {int} date The date where picture was taken, in milliseconds since 1st January 1970 (Unix time).
	 * @param {LatLng} coords The coordinates where picture was taken.
	 * @param {string} provider The provider of the given picture.
	 * @param {string} [author] The author of the picture.
	 * @param {string} [license] The license of this picture.
	 * @param {string} [detailsUrl] The URL where more details about the picture can be found.
	 * @param {int} [direction] The direction at which picture was taken, in degrees (0° = North, 90° = East, 180° = South, 270° = West).
	 * @param {Object} [osmTags] Tags describing this picture in OSM key/value format
	 * @param {string} [thumbUrl] The URL where you can find a small-sized version of the picture
	 * @param {Object} [details] Other picture details
	 * @param {boolean} [details.isSpherical] Is it a 360° picture ?
	 */
	constructor(pictureUrl, date, coords, provider, author, license, detailsUrl, direction, osmTags, thumbUrl, details) {
		//Mandatory parameters
		if(typeof pictureUrl === "string" && pictureUrl.length > 0) {
			/** The picture URL. */
			this.pictureUrl = pictureUrl;
		}
		else {
			throw new Error("model.picture.invalid.pictureUrl");
		}
		
		if(typeof date === "number" && !isNaN(date)) {
			//Check format of date
			try {
				new Date(date);
				
				/** The date where picture was taken, in milliseconds since 1st January 1970 (Unix time). */
				this.date = date;
			}
			catch(e) {
				throw new Error("model.picture.invalid.date");
			}
		}
		else {
			throw new Error("model.picture.invalid.date");
		}
		
		if(coords !== null && coords !== undefined && typeof coords.lat === "number" && typeof coords.lng === "number") {
			/** The coordinates where picture was taken. */
			this.coordinates = coords;
		}
		else {
			throw new Error("model.picture.invalid.coords");
		}
		
		if(typeof provider === "string" && provider.length > 0) {
			/** The provider of the given picture. */
			this.provider = provider;
		}
		else {
			throw new Error("model.picture.invalid.provider");
		}
		
		//Optional parameters
		/** The author of the picture. */
		this.author = author || null;
		/** The license of this picture. */
		this.license = license || null;
		/** The URL where more details about the picture can be found. */
		this.detailsUrl = detailsUrl || null;
		/** The direction at which picture was taken, in degrees (0° = North, 90° = East, 180° = South, 270° = West). */
		this.direction = (typeof direction === "number" && direction >=0 && direction < 360) ? direction : null;
		/** Tags describing this picture in OSM key/value format */
		this.osmTags = osmTags || null;
		/** The URL where thumbnail can be found */
		this.thumbUrl = thumbUrl || null;
		/** The other details about the picture **/
		this.details = details || {};
	}

//OTHER METHODS
	/**
	 * Test if two pictures seems very similar (regarding timestamp, location and direction)
	 * @param {Picture} other The picture to compare with the current one
	 * @return {boolean} True if look alike
	 */
	lookAlike(other) {
		return this.coordinates.equals(other.coordinates) && this.date === other.date && this.direction === other.direction;
	}
}

module.exports = Picture;
