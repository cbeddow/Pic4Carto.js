/*
 * This file is part of Pic4Carto.js.
 * 
 * Pic4Carto.js is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto.js is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.js.  If not, see <http://www.gnu.org/licenses/>.
 */

(function() {
	require("./ctrl/Compatibility");
	
	/**
	 * P4C is a bundle for all classes of the library.
	 */
	let P4C = {
		/** @see {@link #EventLauncher} */
		EventLauncher: require("./ctrl/EventLauncher"),
		/** @see {@link #Detection} */
		Detection: require("./model/Detection"),
		/** @see {@link #Fetcher} */
		Fetcher: require("./ctrl/Fetcher"),
		/** Container for all fetchers. */
		fetchers: {
			/** @see {@link #CSV} */
			CSV: require("./ctrl/fetchers/CSV"),
			/** @see {@link #Flickr} */
			Flickr: require("./ctrl/fetchers/Flickr"),
			/** @see {@link #Mapillary} */
			Mapillary: require("./ctrl/fetchers/Mapillary"),
			/** @see {@link #OpenStreetCam} */
			OpenStreetCam: require("./ctrl/fetchers/OpenStreetCam"),
			/** @see {@link #WikiCommons} */
			WikiCommons: require("./ctrl/fetchers/WikiCommons")
		},
		/** @see {@link #LatLng} */
		LatLng: require("./model/LatLng"),
		/** @see {@link #LatLngBounds} */
		LatLngBounds: require("./model/LatLngBounds"),
		/** @see {@link #Picture} */
		Picture: require("./model/Picture"),
		/** @see {@link #PicturesManager} */
		PicturesManager: require("./ctrl/PicturesManager")
	};
	
	if (typeof module !== 'undefined' && typeof module.exports !== 'undefined')
		module.exports = P4C;
	else
		window.P4C = P4C;
})();
