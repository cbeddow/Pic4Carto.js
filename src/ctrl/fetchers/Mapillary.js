/*
 * This file is part of Pic4Carto.
 *
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

const Fetcher = require("../Fetcher");
const LatLng = require("../../model/LatLng");
const LatLngBounds = require("../../model/LatLngBounds");
const Picture = require("../../model/Picture");
const Detection = require("../../model/Detection");

const MAX_PICS = 100;

// Transform standard detection types into Mapillary types
const DTC_TO_MPL = {
	[Detection.OBJECT_BENCH]: "object--bench",
	[Detection.SIGN_STOP]: [
		"regulatory--stop--g1",
		"regulatory--stop--g2",
		"regulatory--stop--g3",
		"regulatory--stop--g4",
		"regulatory--stop--g5",
		"regulatory--stop--g6",
		"regulatory--stop--g7",
		"regulatory--stop--g8",
		"regulatory--stop--g9",
		"regulatory--stop--g10"
	],
	[Detection.MARK_CROSSING]: [ "marking--discrete--crosswalk-zebra", "construction--flat--crosswalk-plain" ],
	[Detection.OBJECT_BICYCLE_PARKING]: "object--bike-rack",
	[Detection.OBJECT_CCTV]: [ "object--cctv-camera", "information--camera--g1", "information--camera--g2", "information--camera--g3" ],
	[Detection.OBJECT_HYDRANT]: "object--fire-hydrant",
	[Detection.OBJECT_POSTBOX]: "object--mailbox",
	[Detection.OBJECT_MANHOLE]: "object--manhole",
	[Detection.OBJECT_PARKING_METER]: "object--parking-meter",
	[Detection.OBJECT_PHONE]: [ "object--phone-booth", "information--telephone--g1", "information--telephone--g2" ],
	[Detection.SIGN_ADVERT]: "object--sign--advertisement",
	[Detection.SIGN_INFO]: "object--sign--information",
	[Detection.SIGN_STORE]: "object--sign--store",
	[Detection.OBJECT_STREET_LIGHT]: "object--street-light",
	[Detection.OBJECT_POLE]: [ "object--support--pole", "object--support--utility-pole" ],
	[Detection.SIGN_RESERVED_PARKING]: [
		"regulatory--reserved-parking--g1",
		"information--disabled-persons--g1",
		"information--disabled-persons--g2",
		"information--disabled-persons--g3",
		"complementary--disabled-persons--g1"
	],
	[Detection.SIGN_ANIMAL_CROSSING]: [
		"warning--camel-crossing--g1",
		"warning--camel-crossing--g2",
		"warning--domestic-animals--g1",
		"warning--domestic-animals--g2",
		"warning--domestic-animals--g3",
		"warning--domestic-animals--g4",
		"warning--domestic-animals--g5",
		"warning--domestic-animals--g6",
		"warning--domestic-animals--g7",
		"warning--domestic-animals--g8",
		"warning--elephant-crossing--g1",
		"warning--frog-crossing--g1",
		"warning--horse-crossing--g1",
		"warning--kangaroo-crossing--g1",
		"warning--kiwi-crossing--g1",
		"warning--kiwi-crossing--g2",
		"warning--koala-crossing--g1",
		"warning--koala-crossing--g2",
		"warning--koala-crossing--g3",
		"warning--koala-crossing--g4",
		"warning--monkey-crossing--g1",
		"warning--panda-crossing--g1",
		"warning--rabbit-crossing--g1",
		"warning--raccoon-crossing--g1",
		"warning--wild-animals--g1",
		"warning--wild-animals--g2",
		"warning--wild-animals--g3",
		"warning--wild-animals--g4",
		"warning--wild-animals--g5",
		"warning--wild-animals--g6",
		"warning--wild-animals--g7",
		"warning--wild-animals--g8",
		"warning--wombat-crossing--g1"
	],
	[Detection.SIGN_RAILWAY_CROSSING]: [
		"warning--railroad-crossing-with-barriers--g1",
		"warning--railroad-crossing-with-barriers--g2",
		"warning--railroad-crossing-with-barriers--g3",
		"warning--railroad-crossing-with-barriers--g4",
		"warning--railroad-crossing-with-barriers--g5",
		"warning--railroad-crossing-with-barriers--g6",
		"warning--railroad-crossing-with-barriers--g7",
		"warning--railroad-crossing-without-barriers--g1",
		"warning--railroad-crossing-without-barriers--g2",
		"warning--railroad-crossing-without-barriers--g3",
		"warning--railroad-crossing-without-barriers--g4",
		"warning--railroad-crossing-without-barriers--g5",
		"warning--railroad-crossing-without-barriers--g6",
		"warning--railroad-intersection--g1",
		"warning--railroad-intersection--g2",
		"warning--railroad-intersection--g3",
		"warning--railroad-intersection--g4",
		"warning--railroad-intersection--g5",
		"warning--railroad-intersection--g6",
		"warning--railroad-intersection--g7",
		"warning--railroad-intersection--g8",
		"warning--railroad-intersection--g9"
	]
};

// Transform mapillary types into standard types
const MPL_TO_DTC = {};
Object.entries(DTC_TO_MPL).forEach(e => {
	if(typeof e[1] === "string") {
		MPL_TO_DTC[e[1]] = parseInt(e[0]);
	}
	else {
		e[1].forEach(v => { MPL_TO_DTC[v] = parseInt(e[0]); });
	}
});

/**
 * Mapillary fetcher.
 * @example
 * <caption>Credentials for Mapillary Fetcher (see PicturesManager)</caption>
 * fetcherCredentials: {
 * 	mapillary: { key: "APIKEY" }
 * }
 */
class Mapillary extends Fetcher {
//CONSTRUCTOR
	/**
	 * Class constructor.
	 * @param clientId The client ID for the Mapillary API
	 */
	constructor(clientId) {
		super();
		if(typeof clientId === "string" && clientId.length > 0) {
			this.clientId = clientId;
		}
		else {
			throw new Error("ctrl.fetchers.mapillary.invalidclientid");
		}
	}

//ACCESSORS
	/**
	 * Get this provider name.
	 * @return {string} The provider human-readable name.
	 */
	get name() {
		return "Mapillary";
	}

	/**
	 * Get this provider logo URL.
	 * @return {string} The provider logo URL.
	 */
	get logoUrl() {
		return "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Mapillary_logo.svg/480px-Mapillary_logo.svg.png";
	}

	/**
	 * Get this provider homepage URL.
	 * @return {string} The provider homepage URL.
	 */
	get homepageUrl() {
		return "https://www.mapillary.com/";
	}

//OTHER METHODS
	/**
	 * Get all the images in the given area for this provider.
	 * To get pictures when ready, suscribe to the {@link #fetcherdonepictures|donepictures} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get pictures.
	 * @param {Object} [options] The options for pictures retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures as an array.
	 */
	requestPictures(boundingBox, options) {
		let data = Object.assign({
			picsPerRequest: MAX_PICS,
			page: 1,
			pictures: [],
			bbox: new LatLngBounds(boundingBox.getSouthWest().wrap(), boundingBox.getNorthEast().wrap())
		}, this.options, options);

		return this.download(data);
	}

	/**
	 * Get summary statistics about pictures available in the given area.
	 * To get summary when ready, suscribe to the {@link #fetcherdonesummary|donesummary} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get the summary.
	 * @param {Object} [options] The options for summary retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures summary
	 */
	requestSummary(boundingBox, options) {
		options = Object.assign({}, this.options, options);

		let url = "https://a.mapillary.com/v3/images?client_id="
					+ this.clientId
					+ "&bbox=" + boundingBox.getWest() + "," + boundingBox.getSouth() + "," + boundingBox.getEast() + "," + boundingBox.getNorth()
					+ ((options.mindate != null) ? "&start_time=" + (new Date(options.mindate)).toISOString() : "")
					+ ((options.maxdate != null) ? "&end_time=" + (new Date(options.maxdate)).toISOString() : "");

		return this.ajax(url, "json")
		.then(data => {
			if(data === null || data.type !== "FeatureCollection" || data.features === undefined) {
				this.fail(null, null, new Error("ctrl.fetcher.mapillary.getsummaryfailed"));
			}

			//Parse data
			if(data.features && data.features.length > 0) {
				//Search most recent picture date
				data.features.sort((a,b) => { return (new Date(b.properties.captured_at)).getTime() - (new Date(a.properties.captured_at)).getTime(); });

				return {
					last: (new Date(data.features[0].properties.captured_at)).getTime(),
					amount: (data.features.length < MAX_PICS) ? "e"+data.features.length : ">"+MAX_PICS,
					bbox: boundingBox.toBBoxString()
				};
			}
			else {
				return {
					amount: "e0",
					bbox: boundingBox.toBBoxString()
				};
			}
		});
	}

	/**
	 * Get feature detections in the given area.
	 * To get detections when ready, suscribe to the {@link #fetcherdonedetections|donedetections} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get the detections.
	 * @param {object} [options] The options for detections retrieval.
	 * @param {int[]} [options.types] The list of detections types to look for (use Detection.* constants).
	 * @param {Object} [options.prev] Used when retrieving paginated results (nextUrl + data)
	 * @return {Promise} A promise resolving on detections
	 */
	requestDetections(boundingBox, options) {
		options = Object.assign({}, options);

		let url = options.prev ? options.prev.nextUrl : "https://a.mapillary.com/v3/map_features?"
			+ "client_id=" + this.clientId
			+ "&layers=trafficsigns,points"
			+ "&bbox=" + boundingBox.getWest() + "," + boundingBox.getSouth() + "," + boundingBox.getEast() + "," + boundingBox.getNorth()
			+ "&per_page=1000&sort_by=key";

		if(options.types && !options.prev) {
			url += "&values=" + options.types
				.map(t => DTC_TO_MPL[t])
				.map(t => typeof t === "string" ? t : t.join(","))
				.join(",");
		}

		return this.ajax(url, "json_h")
		.then(d => {
			const data = d.json;
			const linkHeader = d.xmlhttp.getResponseHeader("Link");

			if(data === null || data.type !== "FeatureCollection" || data.features === undefined) {
				this.fail(null, null, new Error("ctrl.fetcher.mapillary.getsummaryfailed"));
			}
			else {
				const result = options.prev ? options.prev.data : [];

				data.features.forEach(f => {
					const p = f.properties;

					if(f.geometry.type === "Point" && p.last_seen_at && p.value) {
						result.push(new Detection(
							MPL_TO_DTC[p.value],
							new LatLng(f.geometry.coordinates[1], f.geometry.coordinates[0]),
							(new Date(p.last_seen_at)).getTime(),
							"mapillary"
						));
					}
				});

				//Check if there are more results
				const linkHeaders = linkHeader.split(',');
				const lastLinkHeader = linkHeaders[linkHeaders.length-1].split(';');
				const lastLinkType = lastLinkHeader[1].trim();
				let nextLink = lastLinkHeader[0].trim();
				nextLink = nextLink ? nextLink.substring(1, nextLink.length-1) : null;

				//If other page, download it
				if(lastLinkType === 'rel="next"' && nextLink && nextLink.length > 0) {
					return this.requestDetections(boundingBox, Object.assign({}, options, { prev: { data: result, nextUrl: nextLink } }));
				}
				//If no other page, return result
				else {
					return result;
				}
			}
		});
	}

	/**
	 * Get a list of pictures URL according to given OSM tags.
	 * @param {Object} tags The OSM tags to interpret.
	 * @return {string[]} The list of pictures URL read from tags.
	 */
	tagsToPictures(tags) {
		const result = [];

		Object.keys(tags).filter(k => k.startsWith("mapillary")).forEach(k => {
			tags[k].split(";").forEach(mid => {
				if(/^[0-9A-Za-z_\-]{22}$/.test(mid.trim())) {
					result.push("https://d1cuyjsrcm0gby.cloudfront.net/"+mid.trim()+"/thumb-2048.jpg");
				}
			});
		});

		return result;
	}

	/**
	 * Retrieves pictures metadata for current bounding box, page and date.
	 * @private
	 */
	download(data) {
		let url = "https://a.mapillary.com/v3/images_computed?client_id="
					+ this.clientId
					+ "&bbox=" + data.bbox.getWest() + "," + data.bbox.getSouth() + "," + data.bbox.getEast() + "," + data.bbox.getNorth()
					+ "&limit=" + data.picsPerRequest
					+ ((data.mindate != null) ? "&start_time=" + (new Date(data.mindate)).toISOString() : "")
					+ ((data.maxdate != null) ? "&end_time=" + (new Date(data.maxdate)).toISOString() : "")
					+ "&page=" + data.page
					+ "&per_page=" + data.picsPerRequest
					+ (data.nextStartKey ? "&_start_key=" + data.nextStartKey : "")
					+ (data.nextStartKeyTime ? "&_start_key_time=" + data.nextStartKeyTime : "");

		if(data.nextLink) { url = data.nextLink; }

		return this.ajax(url, "json_h")
		.then(d => {
			const result = d.json;
			const linkHeader = d.xmlhttp.getResponseHeader("Link");

			if(result === null || result.type !== "FeatureCollection" || result.features === undefined) {
				throw new Error("ctrl.fetcher.mapillary.getpicturesfailed");
			}

			//Parse result
			if(result.features && result.features.length > 0) {
				for(let pic of result.features) {
					data.pictures.push(new Picture(
						"https://d1cuyjsrcm0gby.cloudfront.net/" + pic.properties.key + "/thumb-2048.jpg",
						(new Date(pic.properties.captured_at)).getTime(),
						new LatLng(pic.geometry.coordinates[1], pic.geometry.coordinates[0]),
						this.name,
						pic.properties.username,
						"CC By-SA 4.0",
						"https://www.mapillary.com/app/?pKey=" + pic.properties.key + "&lat=" + pic.geometry.coordinates[1] + "&lng=" + pic.geometry.coordinates[0] + "&focus=photo",
						pic.properties.ca,
						{ mapillary: pic.properties.key },
						"https://d1cuyjsrcm0gby.cloudfront.net/" + pic.properties.key + "/thumb-640.jpg",
						{ isSpherical: pic.properties.pano }
					));
				}

				data.page = (linkHeader.indexOf('rel="next"') >= 0) ? data.page + 1 : -1;
			}
			else {
				data.page = -1;
			}

			//Next step
			if(data.page >= 1) {
				let linkHeaders = linkHeader.split(',');
				let nextLink = linkHeaders[linkHeaders.length-1].split(';')[0].trim();
				nextLink = nextLink ? nextLink.substring(1, nextLink.length-1) : null;
				if(nextLink && nextLink.length > 0) {
					data.nextLink = nextLink;
					return this.download(data);
				}
				else {
					return data.pictures;
				}
			}
			else {
				return data.pictures;
			}
		});
	}
}

module.exports = Mapillary;
