/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A fetcher is an object which will download all pictures of a given provider, in a given area.
 * This is an abstract class, it must be extended and the methods overridden.
 */
class Fetcher {
//CONSTRUCTOR
	constructor() {
		if(this.constructor.name === "Fetcher") {
			throw new TypeError("ctrl.fetcher.cantinstantiate");
		}
		
		if(this.requestPictures === undefined) {
			throw new TypeError("ctrl.fetcher.mustoverride.requestPictures");
		}
		
		if(this.requestSummary === undefined) {
			throw new TypeError("ctrl.fetcher.mustoverride.requestSummary");
		}
		
		if(this.tagsToPictures === undefined) {
			throw new TypeError("ctrl.fetcher.mustoverride.tagsToPictures");
		}
		
		if(this.name === undefined) {
			throw new TypeError("ctrl.fetcher.mustoverride.getName");
		}
		
		if(this.logoUrl === undefined) {
			throw new TypeError("ctrl.fetcher.mustoverride.getLogoUrl");
		}
		
		if(this.homepageUrl === undefined) {
			throw new TypeError("ctrl.fetcher.mustoverride.getHomepageUrl");
		}
		
		this.options = {
			mindate: null,
			maxdate: null
		};
	}

//ACCESSORS
	/**
	 * Get this provider name.
	 * @return {string} The provider human-readable name.
	 * @abstract
	 */
	get name() {
		throw new TypeError("ctrl.fetcher.mustoverride.getName");
	}

//OTHER METHODS
	/**
	 * Get all the images in the given area for this provider.
	 * To get pictures when ready, suscribe to the {@link #fetcherdonepictures|donepictures} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get pictures.
	 * @param {object} [options] The options for pictures retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures as an array.
	 * @abstract
	 */
	requestPictures(boundingBox, options) {
		throw new TypeError("ctrl.fetcher.mustoverride.requestPictures");
	}
	
	/**
	 * Get summary statistics about pictures available in the given area.
	 * To get summary when ready, suscribe to the {@link #fetcherdonesummary|donesummary} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get the summary.
	 * @param {object} [options] The options for summary retrieval.
	 * @param {int} [options.mindate] The minimal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @param {int} [options.maxdate] The maximal capture date pictures should have, in milliseconds since 1st January 1970 (Unix time).
	 * @return {Promise} A promise resolving on pictures summary
	 * @abstract
	 */
	requestSummary(boundingBox, options) {
		throw new TypeError("ctrl.fetcher.mustoverride.requestSummary");
	}
	
	/**
	 * Get feature detections in the given area.
	 * To get detections when ready, suscribe to the {@link #fetcherdonedetections|donedetections} event.
	 * @param {LatLngBounds} boundingBox The bounding box of the area where you want to get the detections.
	 * @param {object} [options] The options for detections retrieval.
	 * @param {int[]} [options.types] The list of detections types to look for (use Detection.* constants).
	 * @return {Promise} A promise resolving on detections
	 * @abstract
	 */
	requestDetections(boundingBox, options) {
		throw new TypeError("ctrl.fetcher.mustoverride.requestDetections");
	}
	
	/**
	 * Get a list of pictures URL according to given OSM tags.
	 * @param {Object} tags The OSM tags to interpret.
	 * @return {string[]} The list of pictures URL read from tags.
	 * @abstract
	 */
	tagsToPictures(tags) {
		throw new TypeError("ctrl.fetcher.mustoverride.tagsToPictures");
	}
	
	/**
	 * Creates an Ajax GET request.
	 * @param {string} url The URL of the data to retrieve.
	 * @param {string} type The expected file format.
	 * @return {Promise} A promise, solving on downloaded data, and rejecting on errors
	 */
	ajax(url, type) {
		return new Promise((resolve, reject) => {
			let xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = () => {
				if(xmlhttp.readyState === XMLHttpRequest.DONE) {
					if(xmlhttp.status === 200) {
						try {
							if(type == "json") {
								resolve(JSON.parse(xmlhttp.responseText));
							}
							else if(type == "json_h") {
								resolve({ json: JSON.parse(xmlhttp.responseText), xmlhttp: xmlhttp });
							}
							else {
								resolve(xmlhttp.responseText);
							}
						}
						catch(e) {
							reject(e);
						}
					}
					else {
						reject(new Error("fetcher.ajax.fail "+xmlhttp.status));
					}
				}
			};
			xmlhttp.open("GET", url, true);
			xmlhttp.timeout = 30000;
			xmlhttp.ontimeout = () => { reject(new Error("fetcher.ajax.timeout")); };
			xmlhttp.send();
		});
	}
	
	/**
	 * Creates an Ajax POST request.
	 * @param {string} url The URL of the data to retrieve.
	 * @param {object} [parameters] The HTTP parameters.
	 * @param {string} responseType The expected file format.
	 * @return {Promise} A promise, solving on downloaded data, and rejecting on errors
	 */
	ajaxPost(url, parameters, responseType) {
		return new Promise((resolve, reject) => {
			let xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = () => {
				if(xmlhttp.readyState === XMLHttpRequest.DONE) {
					if(xmlhttp.status === 200) {
						if(responseType == "json") {
							try {
								resolve(JSON.parse(xmlhttp.responseText));
							}
							catch(e) {
								reject(e);
							}
						}
						else {
							resolve(xmlhttp.responseText);
						}
					}
					else {
						reject(new Error("fetcher.ajaxpost.fail"));
					}
				}
			};
			
			//Convert parameters object into a string of HTTP parameters
			let params = "";
			if(parameters) {
				for(const key in parameters) {
					if(params != "") {
						params += "&";
					}
					params += key + "=" + encodeURIComponent(parameters[key]);
				}
			}
			
			//Launch request
			xmlhttp.open("POST", url, true);
			xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			xmlhttp.timeout = 30000;
			xmlhttp.ontimeout = () => { reject(new Error("fetcher.ajax.timeout")); };
			xmlhttp.send(params);
		});
	}
}

module.exports = Fetcher;
