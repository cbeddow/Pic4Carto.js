/*
 * Test script for model/LatLng.js
 */

const assert = require('assert');
const LatLng = require("../../src/model/LatLng");

describe("Model > LatLng", function() {
	describe("Constructor", function() {
		it("sets lat and lng", () => {
			const a = new LatLng(25, 74);
			assert.equal(a.lat, 25);
			assert.equal(a.lng, 74);
			
			const b = new LatLng(-25, -74);
			assert.equal(b.lat, -25);
			assert.equal(b.lng, -74);
		});

		it('throws an error if invalid lat or lng', () => {
			assert.throws(
				() => { const a = new LatLng(NaN, NaN); },
				Error,
				"Invalid LatLng object: (NaN, NaN)"
			);
		});

		it('does not set altitude if undefined', () => {
			const a = new LatLng(25, 74);
			assert.equal(typeof a.alt, 'undefined');
		});

		it('sets altitude', () => {
			const a = new LatLng(25, 74, 50);
			assert.equal(a.alt, 50);
			
			const b = new LatLng(-25, -74, -50);
			assert.equal(b.alt, -50);
		});
	});

	describe("distanceTo", function() {
		it("calculates distance", () => {
			const p1 = new LatLng(36.12, -86.67);
			const p2 = new LatLng(33.94, -118.40);
			assert.ok(p1.distanceTo(p2) >= 2886444.43);
			assert.ok(p1.distanceTo(p2) <= 2886444.45);
		});
	});
});
