/**
 * Test script for PicturesManager
 */

global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
global.XMLHttpRequest.DONE = 4;

const assert = require('assert');
const express = require('express');
const LatLng = require("../../src/model/LatLng");
const LatLngBounds = require("../../src/model/LatLngBounds");
const Picture = require("../../src/model/Picture");
const Detection = require("../../src/model/Detection");
const PicturesManager = require("../../src/ctrl/PicturesManager");

const REQUEST_TIMEOUT = 30000;
const API_PORT = 3001;

const setupServer = () => {
	const app = express();
	app.use(express.static('test/assets'));
	return app.listen(API_PORT);
};

describe("Ctrl > PicturesManager", function() {
	describe("Constructor", function() {
		it("handles user-defined fetchers", function() {
			const pm1 = new PicturesManager({
				userDefinedFetchers: {
					f1: {
						csv: "http://server1.net/index.csv",
						name: "F1",
						logo: "http://server1.net/logo.png",
						homepage: "http://server1.net/",
						bbox: new LatLngBounds(new LatLng(0,0), new LatLng(10,10)),
						license: "Custom license",
						user: "Custom user"
					},
					f2: {
						csv: "http://server2.net/index.csv"
					}
				}
			});

			assert.equal(pm1.fetchers.f1.csvURL, "http://server1.net/index.csv");
			assert.equal(pm1.fetchers.f1.name, "F1");
			assert.equal(pm1.fetchers.f1.logoUrl, "http://server1.net/logo.png");
			assert.equal(pm1.fetchers.f1.homepageUrl, "http://server1.net/");
			assert.ok(pm1.fetchers.f1.options.bbox.equals(new LatLngBounds(new LatLng(0,0), new LatLng(10,10))));
			assert.equal(pm1.fetchers.f1.options.license, "Custom license");
			assert.equal(pm1.fetchers.f1.options.user, "Custom user");

			assert.equal(pm1.fetchers.f2.csvURL, "http://server2.net/index.csv");
		});

		it("fails if udf misses csv parameter", function() {
			assert.throws(
				() => {
					const pm1 = new PicturesManager({
						userDefinedFetchers: {
							f1: {
								name: "F1",
								logo: "http://server1.net/logo.png",
								homepage: "http://server1.net/",
								bbox: new LatLngBounds(new LatLng(0,0), new LatLng(10,10)),
								license: "Custom license",
								user: "Custom user"
							}
						}
					});
				},
				Error,
				"Can't create user-defined fetcher f1: invalid csv parameter (must be an URL)"
			);
		});
	});

	describe("getFetcherDetails", function() {
		it("returns fetchers details", () => {
			const dm1 = new PicturesManager({
				userDefinedFetchers: {
					f1: {
						name: "F1",
						csv: "http://server1.net/index.csv",
						logo: "http://server1.net/logo.png",
						homepage: "http://server1.net/",
						bbox: new LatLngBounds(new LatLng(0,0), new LatLng(10,10)),
						license: "Custom license",
						user: "Custom user"
					}
				}
			});
			const result = dm1.getFetcherDetails();
			const rgxUrl = /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/;

			assert.equal(Object.keys(result).length, 5);

			assert.equal(result.mapillary.name, "Mapillary");
			assert.ok(rgxUrl.test(result.mapillary.homepageUrl));
			assert.ok(rgxUrl.test(result.mapillary.logoUrl));

			assert.equal(result.flickr.name, "Flickr");
			assert.ok(rgxUrl.test(result.flickr.homepageUrl));
			assert.ok(rgxUrl.test(result.flickr.logoUrl));

			assert.equal(result.openstreetcam.name, "OpenStreetCam");
			assert.ok(rgxUrl.test(result.openstreetcam.homepageUrl));
			assert.ok(rgxUrl.test(result.openstreetcam.logoUrl));

			assert.equal(result.wikicommons.name, "Wikimedia Commons");
			assert.ok(rgxUrl.test(result.wikicommons.homepageUrl));
			assert.ok(rgxUrl.test(result.wikicommons.logoUrl));

			assert.equal(result.f1.name, "F1");
			assert.ok(rgxUrl.test(result.f1.homepageUrl));
			assert.ok(rgxUrl.test(result.f1.logoUrl));
		});
	});

	describe("startPicsRetrieval", function() {
		it("over not empty area", () => {
			const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
			const dm1 = new PicturesManager();

			return dm1.startPicsRetrieval(bbox)
			.then(picsArray => {
				assert.ok(picsArray.length > 0);

				for(let i=0; i < picsArray.length; i++) {
					const pic = picsArray[i];
					assert.ok(pic instanceof Picture);
					assert.ok(bbox.contains(pic.coordinates));
					assert.ok(pic.date > 0);
				}
			});
		}).timeout(REQUEST_TIMEOUT);

		it("over empty area", () => {
			const bbox = new LatLngBounds(new LatLng(35.75, -44.78), new LatLng(35.76, -44.77));
			const dm1 = new PicturesManager();

			return dm1.startPicsRetrieval(bbox)
			.then(picsArray => {
				assert.equal(picsArray.length, 0);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("over not empty area and options given", () => {
			const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
			const dm1 = new PicturesManager();

			return dm1.startPicsRetrieval(bbox, { mindate: 1467377425000, maxdate: 1475326225000, cameraAngle: 120 })
			.then(picsArray => {
				assert.ok(picsArray.length > 0);

				for(let i=0; i < picsArray.length; i++) {
					const pic = picsArray[i];
					assert.ok(pic instanceof Picture);
					assert.ok(bbox.contains(pic.coordinates));
					assert.ok(pic.date >= 1467377425000);
					assert.ok(pic.date <= 1475326225000);
				}
			});
		}).timeout(REQUEST_TIMEOUT);

		it("over not empty area and options given (Paris case)", () => {
			const bbox = new LatLngBounds(new LatLng(48.85,2.3), new LatLng(48.855,2.305));
			const dm1 = new PicturesManager();

			return dm1.startPicsRetrieval(bbox, { mindate: 1488326400000 })
			.then(picsArray => {
				assert.ok(picsArray.length > 0);

				for(let i=0; i < picsArray.length; i++) {
					const pic = picsArray[i];
					assert.ok(pic instanceof Picture);
					assert.ok(bbox.contains(pic.coordinates));
					assert.ok(pic.date >= 1488326400000);
				}
			});
		}).timeout(REQUEST_TIMEOUT);

		it("over not empty area and usefetchers option defined", () => {
			const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
			const dm1 = new PicturesManager();
			const fetchers = [ "flickr", "wikicommons" ];

			return dm1.startPicsRetrieval(bbox, { usefetchers: fetchers })
			.then(picsArray => {
				assert.ok(picsArray.length > 0);

				for(let i=0; i < picsArray.length; i++) {
					const pic = picsArray[i];
					assert.ok(pic instanceof Picture);
					assert.ok(bbox.contains(pic.coordinates));
					assert.ok(pic.provider == "Flickr" || pic.provider == "Wikimedia Commons");
				}
			});
		}).timeout(REQUEST_TIMEOUT);

		it("over not empty area and usefetchers too restrictive", () => {
			const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
			const dm1 = new PicturesManager();
			const fetchers = [ "nope" ];

			assert.throws(
				function() {
					dm1.startPicsRetrieval(bbox, { usefetchers: fetchers });
				},
				Error,
				"ctrl.picturesmanager.picsretrieval.nofetchersused"
			);
		});

		it("over not empty area and ignorefetchers option defined", () => {
			const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
			const dm1 = new PicturesManager();
			const ignoredFetchers = [ "mapillary" ];

			return dm1.startPicsRetrieval(bbox, { ignorefetchers: ignoredFetchers })
			.then(picsArray => {
				assert.ok(picsArray.length > 0);

				for(let i=0; i < picsArray.length; i++) {
					const pic = picsArray[i];
					assert.ok(pic instanceof Picture);
					assert.ok(bbox.contains(pic.coordinates));
					assert.ok(pic.provider != "Mapillary");
				}
			});
		}).timeout(REQUEST_TIMEOUT);

		it("over not empty area (Congo Flickr bug)", () => {
			const bbox = new LatLngBounds(new LatLng(3.175,18.615), new LatLng(3.18,18.62));
			const dm1 = new PicturesManager();
			const list = [ "flickr" ];

			return dm1.startPicsRetrieval(bbox, { usefetchers: list })
			.then(picsArray => {
				assert.ok(picsArray.length > 0);

				for(let i=0; i < picsArray.length; i++) {
					const pic = picsArray[i];
					assert.ok(pic instanceof Picture);
					assert.ok(bbox.contains(pic.coordinates));
				}
			});
		}).timeout(REQUEST_TIMEOUT);

		it("over not empty area and ignorefetchers too restrictive", () => {
			const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
			const dm1 = new PicturesManager();
			const ignoredFetchers = [ "mapillary", "flickr", "wikicommons", "openstreetcam" ];

			assert.throws(
				function() {
					dm1.startPicsRetrieval(bbox, { ignorefetchers: ignoredFetchers });
				},
				Error,
				"ctrl.picturesmanager.picsretrieval.nofetchersused"
			);
		});

		it("over not empty area and both ignorefetchers and usefetchers options defined", () => {
			const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
			const dm1 = new PicturesManager();
			const usef = [ "mapillary", "wikicommons" ];
			const ignf = [ "mapillary", "flickr" ];

			return dm1.startPicsRetrieval(bbox, { usefetchers: usef, ignorefetchers: ignf })
			.then(picsArray => {
				assert.ok(picsArray.length > 0);
				let mapillaryPics = 0;

				for(let i=0; i < picsArray.length; i++) {
					const pic = picsArray[i];
					assert.ok(pic instanceof Picture);
					assert.ok(bbox.contains(pic.coordinates));
					assert.ok(pic.provider == "Mapillary" || pic.provider == "Wikimedia Commons");
					if(pic.provider == "Mapillary") { mapillaryPics++; }
				}

				assert.ok(mapillaryPics > 0);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("over area with duplicated pictures", () => {
			const bbox = new LatLngBounds(new LatLng(47.265,-2.345), new LatLng(47.27,-2.34));
			const dm1 = new PicturesManager();

			return dm1.startPicsRetrieval(bbox, { mindate: 1492128000000, maxdate: 1492214400000 })
			.then(picsArray => {
				assert.ok(picsArray.length > 0);

				picsArray.sort((a,b) => { return b.date - a.date; });

				for(let i=1; i < picsArray.length; i++) {
					assert.ok(!picsArray[i].lookAlike(picsArray[i-1]));
				}
			});
		}).timeout(REQUEST_TIMEOUT);

		it("works with user defined fetchers", function() {
			const server = setupServer();

			const bbox = new LatLngBounds(new LatLng(0,0), new LatLng(0.01,0.01));
			const dm1 = new PicturesManager({
				userDefinedFetchers: {
					f1: {
						csv: "http://localhost:"+API_PORT+"/csv_fetcher_1.csv",
						name: "F1"
					}
				}
			});

			return dm1.startPicsRetrieval(bbox, { usefetchers: [ "f1" ] })
			.then(picsArray => {
				assert.ok(picsArray.length > 0);

				for(let i=0; i < picsArray.length; i++) {
					const pic = picsArray[i];
					assert.ok(pic instanceof Picture);
					assert.ok(bbox.contains(pic.coordinates));
					assert.ok(pic.date > 0);
					assert.equal(pic.provider, "F1");
				}

				server.close();
			})
			.catch(e => {
				server.close();
				assert.fail(e);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("sends events for watching fetchers progress", function() {
			const bbox = new LatLngBounds(new LatLng(47.265,-2.345), new LatLng(47.27,-2.34));
			const dm1 = new PicturesManager({
				userDefinedFetchers: {
					f1: {
						csv: "http://localhost:"+API_PORT+"/csv_fetcher_1.csv", //Server not launched for testing fetcher failure
						name: "F1"
					}
				}
			});

			let fetcherok = 0;
			let fetcherfail = 0;

			dm1.on("fetcherdone", fid => {
				fetcherok++;
			});

			dm1.on("fetcherfailed", fid => {
				fetcherfail++;
			});

			return dm1.startPicsRetrieval(bbox, { usefetchers: [ "mapillary", "f1" ] })
			.then(picsArray => {
				return new Promise((resolve, reject) => {
					setTimeout(() => {
						assert.ok(picsArray.length > 0);
						assert.equal(fetcherok, 1);
						assert.equal(fetcherfail, 1);
						resolve();
					}, 1000);
				});
			});
		}).timeout(REQUEST_TIMEOUT);

		it("uses globally defined options", () => {
			const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
			const fetchers = [ "flickr", "wikicommons" ];
			const dm1 = new PicturesManager({ usefetchers: fetchers });

			return dm1.startPicsRetrieval(bbox)
			.then(picsArray => {
				assert.ok(picsArray.length > 0);

				for(let i=0; i < picsArray.length; i++) {
					const pic = picsArray[i];
					assert.ok(pic instanceof Picture);
					assert.ok(bbox.contains(pic.coordinates));
					assert.ok(pic.provider == "Flickr" || pic.provider == "Wikimedia Commons");
				}
			});
		}).timeout(REQUEST_TIMEOUT);
	});

	describe("startPicsRetrievalAround", function() {
		it("over not empty area", () => {
			const center = new LatLng(48.1306333, -1.6770553);
			const bbox = new LatLngBounds(new LatLng(48.130539, -1.677209), new LatLng(48.130722, -1.676919));
			const dm1 = new PicturesManager();

			return dm1.startPicsRetrievalAround(center, 10, { mindate: 1451606400000 })
			.then(picsArray => {
				assert.ok(picsArray.length > 0);
				//console.log(picsArray);

				for(let i=0; i < picsArray.length; i++) {
					const pic = picsArray[i];
					assert.ok(pic instanceof Picture);
					assert.ok(bbox.contains(pic.coordinates));
					assert.ok(pic.date > 0);
				}


			});
		}).timeout(REQUEST_TIMEOUT);

		it("works over not empty area and looking to center", () => {
			const center = new LatLng(48.1306333, -1.6770553);
			const bbox = new LatLngBounds(new LatLng(48.130539, -1.677209), new LatLng(48.130722, -1.676919));
			const dm1 = new PicturesManager();

			return dm1.startPicsRetrievalAround(center, 10, { mindate: 1451606400000, towardscenter: true })
			.then(picsArray => {
				assert.ok(picsArray.length > 0);
				//console.log(picsArray);

				for(let i=0; i < picsArray.length; i++) {
					const pic = picsArray[i];
					assert.ok(pic instanceof Picture);
					assert.ok(bbox.contains(pic.coordinates));
					assert.ok(pic.date > 0);
					assert.ok(dm1._canBeSeen(pic.coordinates, center, pic.direction, 70));
				}


			});
		}).timeout(REQUEST_TIMEOUT);

		it("works over not empty area and looking to center for 360° pictures", () => {
			const center = new LatLng(48.11620936351213, -1.686498639686647);
			const bbox = new LatLngBounds(new LatLng(48.1161, -1.6866), new LatLng(48.1163, -1.6849));
			const dm1 = new PicturesManager();

			return dm1.startPicsRetrievalAround(center, 10, { mindate: 1493769600000, maxdate: 1493855999000, towardscenter: true })
			.then(picsArray => {
				assert.ok(picsArray.length > 0);
// 				console.log(picsArray);

				for(let i=0; i < picsArray.length; i++) {
					const pic = picsArray[i];
					assert.ok(pic instanceof Picture);
					assert.ok(bbox.contains(pic.coordinates));
					assert.ok(pic.date > 0);
					assert.ok(pic.details.isSpherical || dm1._canBeSeen(pic.coordinates, center, pic.direction, 70));
				}


			});
		}).timeout(REQUEST_TIMEOUT);
	});

	describe("startSummaryRetrieval", function() {
		it("over not empty area", () => {
			const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
			const dm1 = new PicturesManager();

			return dm1.startSummaryRetrieval(bbox)
			.then(summary => {
				assert.ok(summary.amount > 0);
				assert.ok(summary.approxAmount === true || summary.approxAmount === false);
				assert.ok(summary.last > 0);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("over empty area", () => {
			const bbox = new LatLngBounds(new LatLng(35.75, -44.78), new LatLng(35.76, -44.77));
			const dm1 = new PicturesManager();

			return dm1.startSummaryRetrieval(bbox)
			.then(summary => {
				assert.equal(summary.amount, 0);
				assert.ok(!summary.approxAmount);
				assert.equal(summary.last, 0);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("over not empty area with options", () => {
			const bbox = new LatLngBounds(new LatLng(48.10,-1.69), new LatLng(48.105,-1.685));
			const dm1 = new PicturesManager();

			return dm1.startSummaryRetrieval(bbox)
			.then(summary => {
				assert.ok(summary.amount > 0);
				assert.ok(summary.approxAmount === true || summary.approxAmount === false);
				assert.ok(summary.last > 0);

				return dm1.startSummaryRetrieval(bbox, { mindate: 1467377425000, maxdate: 1475326225000 })
				.then(summary2 => {
					assert.ok(summary2.amount < summary.amount);
					assert.ok(summary2.approxAmount === true || summary2.approxAmount === false);
					assert.ok(summary2.last >= 1467377425000);
					assert.ok(summary2.last <= 1475326225000);
				});
			});
		}).timeout(REQUEST_TIMEOUT);

		it("over not empty area with options and ignore fetchers", () => {
			const bbox = new LatLngBounds(new LatLng(48.13,-1.67), new LatLng(48.135,-1.665));
			const dm1 = new PicturesManager();

			return dm1.startSummaryRetrieval(bbox, { mindate: 1501545600000, maxdate: 1501891200000, ignorefetchers: [ "mapillary" ] })
			.then(summary => {
				assert.equal(summary.amount, 0);
				assert.ok(summary.approxAmount === false);
				assert.equal(summary.last, 0);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("over empty Paris in January 2017", () => {
			const bbox = new LatLngBounds(new LatLng(48.84,2.305), new LatLng(48.845,2.31));
			const dm1 = new PicturesManager();

			return dm1.startSummaryRetrieval(bbox, { mindate: 1483228800000, maxdate: 1484870400000 })
			.then(summary => {
				assert.equal(summary.amount, 0);
				assert.ok(!summary.approxAmount);
				assert.equal(summary.last, 0);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("over empty area in Cuba", () => {
			const bbox = new LatLngBounds(new LatLng(23.155,-81.24), new LatLng(23.16,-81.235));
			const dm1 = new PicturesManager();

			return dm1.startSummaryRetrieval(bbox, { mindate: Date.now()-1000*60*60*24*30*6 })
			.then(summary => {
				assert.equal(summary.amount, 0);
				assert.ok(!summary.approxAmount);
				assert.equal(summary.last, 0);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("sends events for watching fetchers progress", function() {
			const bbox = new LatLngBounds(new LatLng(48.13,-1.67), new LatLng(48.135,-1.665));
			const dm1 = new PicturesManager({
				userDefinedFetchers: {
					f1: {
						csv: "http://localhost:"+API_PORT+"/csv_fetcher_1.csv", //Server not launched for testing fetcher failure
						name: "F1"
					}
				}
			});

			let fetcherok = 0;
			let fetcherfail = 0;

			dm1.on("fetcherdone", fid => {
				fetcherok++;
			});

			dm1.on("fetcherfailed", fid => {
				fetcherfail++;
			});

			return dm1.startSummaryRetrieval(bbox, { mindate: 1501545600000, maxdate: 1501891200000, usefetchers: [ "mapillary", "f1" ] })
			.then(summary => {
				return new Promise((resolve, reject) => {
					setTimeout(() => {
						assert.equal(fetcherok, 1);
						assert.equal(fetcherfail, 1);
						resolve();
					}, 1000);
				});
			});
		}).timeout(REQUEST_TIMEOUT);
	});

	describe("startDetectionsRetrieval", () => {
		const CREDS = { fetcherCredentials: { mapillary: { key: "eDhQTTdnRmZJNXdYZWUwRDQxc1NwdzoyMTgzMTUzNTY5YjI3OWZh" }}};

		it("works over not empty area", () => {
			const bbox = new LatLngBounds(new LatLng(48.12902, -1.67708), new LatLng(48.13101, -1.67515));
			const dm1 = new PicturesManager(CREDS);

			return dm1.startDetectionsRetrieval(bbox, { types: [ Detection.SIGN_STOP ] })
			.then(features => {
				assert.ok(features.length > 0);
				features.forEach(f => {
					assert.ok(f.provider);
					assert.ok(f.date);
					assert.ok(f.coordinates);
					assert.ok(f.type >= 0);
				});
			});
		}).timeout(REQUEST_TIMEOUT);

		it("works over not empty area and result as GeoJSON", () => {
			const bbox = new LatLngBounds(new LatLng(48.12902, -1.67708), new LatLng(48.13101, -1.67515));
			const dm1 = new PicturesManager(CREDS);

			return dm1.startDetectionsRetrieval(bbox, { types: [ Detection.SIGN_STOP ], asgeojson: true })
			.then(geojson => {
				assert.equal(geojson.type, "FeatureCollection");
				assert.ok(geojson.features.length > 0);
				geojson.features.forEach(f => {
					assert.equal(f.geometry.type, "Point");
					assert.equal(f.geometry.coordinates.length, 2);
					assert.ok(Object.keys(f.properties).length > 0);
				});
			});
		}).timeout(REQUEST_TIMEOUT);
	});

	describe("getPicturesFromTags", () => {
		it("works", () => {
			const dm1 = new PicturesManager();
			const tags = {
				"amenity": "place_of_worship",
				"building": "church",
				"denomination": "catholic",
				"description": "Edificio in pietra, riedificato nel 1709. La chiesa primitiva, del 1200, fu distrutta da un fulmine.",
				"historic": "church",
				"image": "File:Chiesetta_di_San_Sfirio.jpg",
				"mapillary": "PnaVMtVwaIWiWRp4-EFzYQ",
				"name": "Chiesetta di San Sfirio",
				"religion": "christian",
				"source": "Database topografico regione Lombardia",
				"start_date": "1709",
				"wikimedia_commons": "File:Chiesetta_di_San_Sfirio.jpg"
			};

			const result = dm1.getPicturesFromTags(tags);
			assert.equal(result.length, 2);
			assert.equal(result[0], "https://d1cuyjsrcm0gby.cloudfront.net/PnaVMtVwaIWiWRp4-EFzYQ/thumb-2048.jpg");
			assert.equal(result[1], "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Chiesetta_di_San_Sfirio.jpg/1024px-Chiesetta_di_San_Sfirio.jpg");
		});
	});

	describe("_canBeSeen", () => {
		it("works on 0,0 N", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(0,0), new LatLng(1,0), 0, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(0,0), new LatLng(1,0), 180, 10);
			assert.ok(!result2);
		});

		it("works on 0,0 NW", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(0,0), new LatLng(1,-1), 315, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(0,0), new LatLng(1,-1), 135, 10);
			assert.ok(!result2);
		});

		it("works on 0,0 W", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(0,0), new LatLng(0,-1), 270, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(0,0), new LatLng(0,-1), 90, 10);
			assert.ok(!result2);
		});

		it("works on 0,0 SW", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(0,0), new LatLng(-1,-1), 225, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(0,0), new LatLng(-1,-1), 45, 10);
			assert.ok(!result2);
		});

		it("works on 0,0 S", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(0,0), new LatLng(-1,0), 180, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(0,0), new LatLng(-1,0), 0, 10);
			assert.ok(!result2);
		});

		it("works on 0,0 SE", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(0,0), new LatLng(-1,1), 135, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(0,0), new LatLng(-1,1), 315, 10);
			assert.ok(!result2);
		});

		it("works on 0,0 E", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(0,0), new LatLng(0,1), 90, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(0,0), new LatLng(0,1), 270, 10);
			assert.ok(!result2);
		});

		it("works on 0,0 NE", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(0,0), new LatLng(1,1), 45, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(0,0), new LatLng(1,1), 225, 10);
			assert.ok(!result2);
		});

		//
		it("works on 1,1 N", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(1,1), new LatLng(2,1), 0, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(1,1), new LatLng(2,1), 180, 10);
			assert.ok(!result2);
		});

		it("works on 1,1 NW", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(1,1), new LatLng(2,0), 315, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(1,1), new LatLng(2,0), 135, 10);
			assert.ok(!result2);
		});

		it("works on 1,1 W", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(1,1), new LatLng(1,0), 270, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(1,1), new LatLng(1,0), 90, 10);
			assert.ok(!result2);
		});

		it("works on 1,1 SW", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(1,1), new LatLng(0,0), 225, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(1,1), new LatLng(0,0), 45, 10);
			assert.ok(!result2);
		});

		it("works on 1,1 S", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(1,1), new LatLng(0,1), 180, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(1,1), new LatLng(0,1), 0, 10);
			assert.ok(!result2);
		});

		it("works on 1,1 SE", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(1,1), new LatLng(0,2), 135, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(1,1), new LatLng(0,2), 315, 10);
			assert.ok(!result2);
		});

		it("works on 1,1 E", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(1,1), new LatLng(1,2), 90, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(1,1), new LatLng(1,2), 270, 10);
			assert.ok(!result2);
		});

		it("works on 1,1 NE", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(1,1), new LatLng(2,2), 45, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(1,1), new LatLng(2,2), 225, 10);
			assert.ok(!result2);
		});

		//
		it("works on -1,-1 N", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(0,-1), 0, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(0,-1), 180, 10);
			assert.ok(!result2);
		});

		it("works on -1,-1 NW", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(0,-2), 315, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(0,-2), 135, 10);
			assert.ok(!result2);
		});

		it("works on -1,-1 W", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-1,-2), 270, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-1,-2), 90, 10);
			assert.ok(!result2);
		});

		it("works on -1,-1 SW", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-2,-2), 225, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-2,-2), 45, 10);
			assert.ok(!result2);
		});

		it("works on -1,-1 S", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-2,-1), 180, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-2,-1), 0, 10);
			assert.ok(!result2);
		});

		it("works on -1,-1 SE", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-2,0), 135, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-2,0), 315, 10);
			assert.ok(!result2);
		});

		it("works on -1,-1 E", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-1,0), 90, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(-1,0), 270, 10);
			assert.ok(!result2);
		});

		it("works on -1,-1 NE", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(0,0), 45, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(-1,-1), new LatLng(0,0), 225, 10);
			assert.ok(!result2);
		});

		//
		it("works on -1,0 N", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(0,0), 0, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(0,0), 180, 10);
			assert.ok(!result2);
		});

		it("works on -1,0 NW", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(0,-1), 315, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(0,-1), 135, 10);
			assert.ok(!result2);
		});

		it("works on -1,0 W", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-1,-1), 270, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-1,-1), 90, 10);
			assert.ok(!result2);
		});

		it("works on -1,0 SW", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-2,-1), 225, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-2,-1), 45, 10);
			assert.ok(!result2);
		});

		it("works on -1,0 S", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-2,0), 180, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-2,0), 0, 10);
			assert.ok(!result2);
		});

		it("works on -1,0 SE", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-2,1), 135, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-2,1), 315, 10);
			assert.ok(!result2);
		});

		it("works on -1,0 E", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-1,1), 90, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(-1,1), 270, 10);
			assert.ok(!result2);
		});

		it("works on -1,0 NE", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(0,1), 45, 10);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(-1,0), new LatLng(0,1), 225, 10);
			assert.ok(!result2);
		});

		it("works on Rennes", () => {
			const dm1 = new PicturesManager();
			const result1 = dm1._canBeSeen(new LatLng(48.11397,-1.68043), new LatLng(48.11377,-1.68072), 225, 100);
			assert.ok(result1);

			const result2 = dm1._canBeSeen(new LatLng(48.11397,-1.68043), new LatLng(48.11447,-1.67925), 225, 100);
			assert.ok(!result2);
		});
	});
});
