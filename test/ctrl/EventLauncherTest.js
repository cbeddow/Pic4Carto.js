/**
 * Test script for EventLauncher
 */

const assert = require('assert');
const EventLauncher = require("../../src/ctrl/EventLauncher");

class Subclass extends EventLauncher {
	constructor() {
		super();
	}
}

describe("Ctrl > EventLauncher", function() {
	describe("Constructor", function() {
		it("Constructor which can't be instantiated", () => {
			assert.throws(
				() => { const el1 = new EventLauncher(); },
				TypeError,
				"ctrl.eventlauncher.cantinstantiate"
			);
		});

		it("Constructor of heriting class", () => {
			assert.ok(new Subclass());
			
			const sc1 = new Subclass();
			assert.ok(sc1.fire);
			assert.ok(sc1.on);
			assert.ok(sc1.off);
		});
	});

	describe("fire", function() {
		it("fire with associated object", (done) => {
			const sc1 = new Subclass();
			
			sc1.on("testfire", (o) => {
				assert.ok(o.defined);
				done();
			});
			
			sc1.fire("testfire", { defined: true });
		});
	});

	describe("on", function() {
		it("on with multiple listeners", (done) => {
			const sc1 = new Subclass();
			const max = 2;
			let i = 0;
			
			sc1.on("testfire", (o) => {
				assert.ok(o.defined);
				i++;
				if(i == max) {
					done();
				}
			});
			
			sc1.on("testfire", (o) => {
				assert.ok(o.test2);
				i++;
				if(i == max) {
					done();
				}
			});
			
			sc1.fire("testfire", { defined: true, test2: true });
		});
	});
	
	describe("off", function() {
		it("off on one listener", (done) => {
			const sc1 = new Subclass();
			const max = 3;
			let i = 0;
			
			const l1 = (o) => {
				assert.ok(o.defined);
				i++;
				if(i == max) {
					done();
				}
			};
			const l2 = (o) => {
				assert.ok(o.test2);
				i++;
				if(i == max) {
					done();
				}
			};
			
			sc1.on("testfire", l1);
			sc1.on("testfire", l2);
			
			sc1.fire("testfire", { defined: true, test2: true });
			
			setTimeout(
				() => {
					sc1.off("testfire", l1);
					sc1.fire("testfire", { defined: true, test2: true });
				},
				100
			);
		});

		it("off on all listeners", (done) => {
			const sc1 = new Subclass();
			
			const l1 = (o) => {
				assert.ok(false);
				done();
			};
			const l2 = (o) => {
				assert.ok(false);
				done();
			};
			
			sc1.on("testfire", l1);
			sc1.on("testfire", l2);
			
			setTimeout(
				() => {
					sc1.off("testfire");
					sc1.fire("testfire", { defined: true, test2: true });
					setTimeout(() => {
							assert.ok(true);
							done();
						},
						200
					);
				},
				100
			);
		});
	});

	it("supports chained calls", (done) => {
		const sc1 = new Subclass();
		
		sc1
			.on("testfire", (o) => {
				assert.ok(o.defined);
				done();
			})
			.fire("testfire", { defined: true });
	});
});
