/*
 * Test script for ctrl/fetchers/Flickr.js
 */

global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
global.XMLHttpRequest.DONE = 4;

const assert = require('assert');
const Flickr = require("../../../src/ctrl/fetchers/Flickr");
const LatLng = require("../../../src/model/LatLng");
const LatLngBounds = require("../../../src/model/LatLngBounds");

const API_KEY = "b8c50865a2c22d93d3e673de8bd50178";
const REQUEST_TIMEOUT = 30000;

describe("Ctrl > Fetchers > Flickr", function() {
	describe("Constructor", function() {
		it("Constructor with valid parameters", () => {
			const m1 = new Flickr(API_KEY);
			
			//Check attributes
			assert.equal(m1.apiKey, API_KEY);
			assert.equal(m1.name, "Flickr");
			assert.ok(/(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.homepageUrl));
			assert.ok(/(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.logoUrl));
		});

		it("Constructor with missing parameters", () => {
			assert.throws(
				function() {
					const m1 = new Flickr();
				},
				Error,
				"ctrl.fetchers.flickr.invalidapikey"
			);
		});
	});

	describe("requestPictures", function() {
		it("requestPictures with valid parameters", () => {
			const m1 = new Flickr(API_KEY);
			const bbox = new LatLngBounds(new LatLng(48.85,2.2899999999999996), new LatLng(48.86,2.2999999999999994));
			
			return m1.requestPictures(bbox)
			.then(pictures => {
				assert.ok(pictures.length > 0);

				for(let i=0; i < pictures.length && i < 20; i++) {
					const pic = pictures[i];
					assert.ok(pic.author.length > 0);
					assert.ok(pic.date <= Date.now());
					assert.ok(pic.date > 0);
					assert.ok(pic.pictureUrl.startsWith("https://live"));
					assert.ok(pic.detailsUrl.startsWith("https://www.flickr.com/photos/"));
					assert.equal(pic.provider, "Flickr");
					assert.ok(bbox.contains(pic.coordinates));
					assert.equal(pic.direction, null);
					assert.equal(pic.osmTags.flickr, pic.detailsUrl);
					assert.ok(pic.thumbUrl.startsWith("https://live"));
				}

			})
			.catch(e => {
				console.log(e);
				assert.ok(false);

			});
		}).timeout(REQUEST_TIMEOUT);

		it("requestPictures with valid parameters on empty area", () => {
			const m1 = new Flickr(API_KEY);
			const bbox = new LatLngBounds(new LatLng(31.410000000000004,-42.43000000000001), new LatLng(31.420000000000005,-42.42000000000001));
			
			return m1.requestPictures(bbox)
			.then(pictures => {
				assert.equal(pictures.length, 0);

			})
			.catch(e => {
				console.log(e);
				assert.ok(false);

			});
		}).timeout(REQUEST_TIMEOUT);

		it("requestPictures with valid parameters and options", () => {
			const m1 = new Flickr(API_KEY);
			const bbox = new LatLngBounds(new LatLng(48.85,2.2899999999999996), new LatLng(48.86,2.2999999999999994));
			
			return m1.requestPictures(bbox, { mindate: 1467377425000, maxdate: 1475326225000 })
			.then(pictures => {
				assert.ok(pictures.length > 0);
				
				for(let i=0; i < pictures.length && i < 20; i++) {
					const pic = pictures[i];
					assert.ok(pic.author.length > 0);
					assert.ok(pic.date <= 1475326225000);
					assert.ok(pic.date >= 1467377425000);
					assert.ok(pic.pictureUrl.startsWith("https://live"));
					assert.ok(pic.detailsUrl.startsWith("https://www.flickr.com/photos/"));
					assert.equal(pic.provider, "Flickr");
					assert.ok(bbox.contains(pic.coordinates));
					assert.equal(pic.direction, null);
				}

			})
			.catch(e => {
				console.log(e);
				assert.ok(false);

			});
		}).timeout(REQUEST_TIMEOUT);
	});
	
	describe("requestSummary", function() {
		it("requestSummary with valid parameters", () => {
			const m1 = new Flickr(API_KEY);
			const bbox = new LatLngBounds(new LatLng(48.85,2.2899999999999996), new LatLng(48.86,2.2999999999999994));
			
			return m1.requestSummary(bbox)
			.then(stats => {
				assert.ok(stats.amount.length > 0);
				assert.ok(/^[e>][0-9]+$/.test(stats.amount));
				assert.ok(stats.last > 0);
				assert.equal(stats.bbox, bbox.toBBoxString());

			})
			.catch(e => {
				console.log(e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("requestSummary with valid parameters and options", () => {
			const m1 = new Flickr(API_KEY);
			const bbox = new LatLngBounds(new LatLng(48.85,2.2899999999999996), new LatLng(48.86,2.2999999999999994));
			
			return m1.requestSummary(bbox, { mindate: 1467377425000, maxdate: 1475326225000 })
			.then(stats => {
				assert.ok(stats.amount.length > 0);
				assert.ok(/^[e>][0-9]+$/.test(stats.amount));
				assert.ok(stats.last >= 1467377425000);
				assert.ok(stats.last <= 1475326225000);
				assert.equal(stats.bbox, bbox.toBBoxString());

			})
			.catch(e => {
				console.log(e);
				assert.ok(false);

			});
		}).timeout(REQUEST_TIMEOUT);
	});
});
