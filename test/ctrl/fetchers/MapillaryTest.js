/*
 * Test script for ctrl/fetchers/Mapillary.js
 */

global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
global.XMLHttpRequest.DONE = 4;

const assert = require('assert');
const LatLng = require("../../../src/model/LatLng");
const LatLngBounds = require("../../../src/model/LatLngBounds");
const Detection = require("../../../src/model/Detection");
const Mapillary = require("../../../src/ctrl/fetchers/Mapillary");

const CLIENT_ID = "eDhQTTdnRmZJNXdYZWUwRDQxc1NwdzoyMTgzMTUzNTY5YjI3OWZh";
const REQUEST_TIMEOUT = 30000;

describe("Ctrl > Fetchers > Mapillary", function() {
	describe("Constructor", function() {
		it("Constructor with valid parameters", () => {
			const m1 = new Mapillary(CLIENT_ID);

			//Check attributes
			assert.equal(m1.clientId, CLIENT_ID);
			assert.equal(m1.name, "Mapillary");
			assert.ok(/(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.homepageUrl));
			assert.ok(/(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.logoUrl));
		});

		it("Constructor with missing parameters", () => {
			assert.throws(
				function() {
					const m1 = new Mapillary();
				},
				Error,
				"ctrl.fetchers.mapillary.invalidclientid"
			);
		});
	});

	describe("requestPictures", function() {
		it("requestPictures with valid parameters", () => {
			const m1 = new Mapillary(CLIENT_ID);
			const bbox = new LatLngBounds(new LatLng(48.12421, -1.72100), new LatLng(48.12961, -1.70733));

			return m1.requestPictures(bbox)
			.then(pictures => {
				assert.ok(pictures.length > 0);

				const pic1 = pictures[1];
				assert.ok(pic1.author.length > 0);
				assert.equal(pic1.license, "CC By-SA 4.0");
				assert.ok(pic1.date > 0);
				assert.ok(pic1.pictureUrl.startsWith("https://d1cuyjsrcm0gby.cloudfront.net/"));
				assert.ok(pic1.detailsUrl.startsWith("https://www.mapillary.com/app/?pKey="));
				assert.equal(pic1.provider, "Mapillary");
				assert.ok(bbox.contains(pic1.coordinates));
				assert.equal(typeof pic1.direction, "number");
				assert.ok(pic1.osmTags.mapillary.match(/^[a-zA-Z0-9_\-]+$/));
				assert.ok(pic1.thumbUrl.startsWith("https://d1cuyjsrcm0gby.cloudfront.net/"));

			})
			.catch(e => {
				console.log("Failed "+e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("requestPictures with valid parameters on empty area", () => {
			const m1 = new Mapillary(CLIENT_ID);
			const bbox = new LatLngBounds(new LatLng(45.30004812466808, -24.53090028975572), new LatLng(46.09329426310072, -22.752314896985553));

			return m1.requestPictures(bbox)
			.then(pictures => {
				assert.equal(pictures.length, 0);

			})
			.catch(e => {
				console.log(e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("requestPictures with valid parameters and options defined", () => {
			const m1 = new Mapillary(CLIENT_ID);
			const bbox = new LatLngBounds(new LatLng(48.12421, -1.72100), new LatLng(48.12961, -1.70733));

			return m1.requestPictures(bbox, { mindate: 1467377425000, maxdate: 1475326225000 })
			.then(pictures => {
				assert.ok(pictures.length > 0);

				const pic1 = pictures[1];
				assert.ok(pic1.author.length > 0);
				assert.equal(pic1.license, "CC By-SA 4.0");
				assert.ok(pic1.date >= 1467377425000);
				assert.ok(pic1.date <= 1475326225000);
				assert.ok(pic1.pictureUrl.startsWith("https://d1cuyjsrcm0gby.cloudfront.net/"));
				assert.ok(pic1.detailsUrl.startsWith("https://www.mapillary.com/app/?pKey="));
				assert.equal(pic1.provider, "Mapillary");
				assert.ok(bbox.contains(pic1.coordinates));
				assert.equal(typeof pic1.direction, "number");
			})
			.catch(e => {
				console.log(e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);
	});

	describe("requestSummary", function() {
		it("requestSummary with valid parameters", () => {
			const m1 = new Mapillary(CLIENT_ID);
			const bbox = new LatLngBounds(new LatLng(48.12421, -1.72100), new LatLng(48.12961, -1.70733));

			return m1.requestSummary(bbox)
			.then(stats => {
				assert.ok(stats.amount.length > 0);
				assert.ok(/^[e>][0-9]+$/.test(stats.amount));
				assert.ok(stats.last > 0);
				assert.equal(stats.bbox, bbox.toBBoxString());
			})
			.catch(e => {
				console.log(e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("requestSummary with valid parameters and options", () => {
			const m1 = new Mapillary(CLIENT_ID);
			const bbox = new LatLngBounds(new LatLng(48.12421, -1.72100), new LatLng(48.12961, -1.70733));

			return m1.requestSummary(bbox, { mindate: 1467377425000, maxdate: 1475326225000 })
			.then(stats => {
				assert.ok(stats.amount.length > 0);
				assert.ok(/^[e>][0-9]+$/.test(stats.amount));
				assert.ok(stats.last >= 1467377425000);
				assert.ok(stats.last <= 1475326225000);
				assert.equal(stats.bbox, bbox.toBBoxString());
			})
			.catch(e => {
				console.log(e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);
	});

	describe("requestDetections", () => {
		it("works with valid parameters", () => {
			const m1 = new Mapillary(CLIENT_ID);
			const bbox = new LatLngBounds(new LatLng(48.12902, -1.67708), new LatLng(48.13101, -1.67515));

			return m1.requestDetections(bbox, { types: [ Detection.SIGN_STOP ]})
			.then(features => {
				assert.ok(features.length > 0);
				features.forEach(f => {
					assert.ok(f instanceof Detection);
					assert.equal(f.provider, "mapillary");
					assert.equal(f.type, Detection.SIGN_STOP);
					assert.ok(f.date >= 1480005836272);
				});
			})
			.catch(e => {
				console.log(e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("works on wide area", () => {
			const m1 = new Mapillary(CLIENT_ID);
			const bbox = new LatLngBounds(new LatLng(48.3216, -1.9611), new LatLng(47.8869, -1.3184));

			return m1.requestDetections(bbox, { types: [ Detection.SIGN_STOP ]})
			.then(features => {
				assert.ok(features.length > 1000);
				features.forEach(f => {
					assert.ok(f instanceof Detection);
					assert.equal(f.provider, "mapillary");
					assert.equal(f.type, Detection.SIGN_STOP);
				});
			})
			.catch(e => {
				console.log(e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);
	});

	describe("tagsToPictures", () => {
		it("works", () => {
			const m1 = new Mapillary(CLIENT_ID);
			const tags = { amenity: "bar", mapillary: "87Fu43CBg7Ls17kUnpRv4w" };
			const result = m1.tagsToPictures(tags);

			assert.equal(result.length, 1);
			assert.equal(result[0], "https://d1cuyjsrcm0gby.cloudfront.net/87Fu43CBg7Ls17kUnpRv4w/thumb-2048.jpg");
		});

		it("ignores invalid tag value", () => {
			const m1 = new Mapillary(CLIENT_ID);
			const tags = { amenity: "bar", mapillary: "Not a mapillary key" };
			const result = m1.tagsToPictures(tags);

			assert.equal(result.length, 0);
		});

		it("handles multiple values", () => {
			const m1 = new Mapillary(CLIENT_ID);
			const tags = { amenity: "bar", mapillary: "87Fu43CBg7Ls17kUnpRv4w;x8cFRs-Zmpmjeb_9SgeuvA", "mapillary:N": "B4N8Yx4R_n4Lp02kRU8T3Q" };
			const result = m1.tagsToPictures(tags);

			assert.equal(result.length, 3);
			assert.equal(result[0], "https://d1cuyjsrcm0gby.cloudfront.net/87Fu43CBg7Ls17kUnpRv4w/thumb-2048.jpg");
			assert.equal(result[1], "https://d1cuyjsrcm0gby.cloudfront.net/x8cFRs-Zmpmjeb_9SgeuvA/thumb-2048.jpg");
			assert.equal(result[2], "https://d1cuyjsrcm0gby.cloudfront.net/B4N8Yx4R_n4Lp02kRU8T3Q/thumb-2048.jpg");
		});
	});
});
